<?php

/**
 * @file
 * Integrate a barcode scanner into Commerce.
 */


// The path used to trigger a barcode scan.
define('COMMERCE_BARCODE_SCANNER_TRIGGER_PATH', 'barcode-scanner/trigger-event');


/**
 * Implements hook_permission().
 */
function commerce_barcode_scanner_permission() {
  return array(
    'administer commerce barcode scanner' => array(
      'title' => t('Administer Commerce Barcode Scanner'),
      'description' => t('Perform administration tasks for my module.'),
    ),
    'trigger commerce barcode events' => array(
      'title' => t('Trigger commerce barcode events'),
      'description' => t('This allows users to trigger any events that are the result of a barcode being scanned.'),
    ),
  );
}


/**
 * Implements hook_menu().
 */
function commerce_barcode_scanner_menu() {
  // The path that triggers our rules event.
  $items[COMMERCE_BARCODE_SCANNER_TRIGGER_PATH . '/%'] = array(
    'title' => 'Trigger Events',
    'page callback' => 'commerce_barcode_scanner_trigger_scan',
    'page arguments' => array(2),
    'access arguments' => array('trigger commerce barcode events'),
    'description' => 'Trigger scanned barcode events.',
    'file' => 'commerce_barcode_scanner.rules.inc',
    'type' => MENU_NORMAL_ITEM,
  );

  // The default local task item.
  $items['admin/commerce/barcode-scanner/config'] = array(
    'title' => 'Barcode Scanner',
    'access arguments' => array('administer commerce barcode scanner'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );

  // The barcode scanner config page.
  $items['admin/commerce/barcode-scanner'] = array(
    'title' => 'Barcode Scanner',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('commerce_barcode_scanner_admin_form'),
    'access arguments' => array('administer commerce barcode scanner'),
    'description' => 'Administer the commerce barcode scanner integration.',
    'file' => 'commerce_barcode_scanner.admin.inc',
    'type' => MENU_NORMAL_ITEM,
  );

  // The rules integration page.
  if (module_exists('rules_admin')) {
    $items['admin/commerce/barcode-scanner/rules'] = array(
      'title' => 'Barcode Scanner Rules',
      'page callback' => 'commerce_barcode_scanner_scanner_rules',
      'access arguments' => array('administer commerce barcode scanner'),
      'description' => 'Administer the rules for Commerce Barcode scanner.',
      'file' => 'commerce_barcode_scanner.rules.inc',
      'type' => MENU_LOCAL_TASK,
    );
  }

  return $items;
}


/**
 * Implements hook_preprocess_page().
 */
function commerce_barcode_scanner_preprocess_page(&$vars) {
  $module_path = drupal_get_path('module', 'commerce_barcode_scanner');
  drupal_add_js($module_path . '/js/barcode-scanner.js', array('every_page' => TRUE));
  drupal_add_js(
    array(
      'commerce_barcode_scanner' => array(
        'trigger_path' => url(COMMERCE_BARCODE_SCANNER_TRIGGER_PATH),
        'min_sku_length' => commerce_barcode_scanner_variable_get('min_sku_length'),
        'key_interval' => commerce_barcode_scanner_variable_get('key_interval'),
      ),
    ),
    array(
      'type' => 'setting',
    )
  );
}


/**
 * Get the configuration variable corresponding to the given key.
 *
 * @param string $key
 *   The config key for the data required.
 *
 * @return string
 *   The config data.
 * @throws InvalidArgumentException
 */
function commerce_barcode_scanner_variable_get($key) {
  $key = 'commerce_barcode_scanner_' . $key;
  $default_variables = commerce_barcode_scanner_default_variables();
  if (!isset($default_variables[$key])) {
    throw new InvalidArgumentException('Invalid config key: ' . $key);
  }
  return variable_get($key, $default_variables[$key]);
}


/**
 * Get an array of all default config.
 *
 * @return array
 *   An array of default configuration.
 */
function commerce_barcode_scanner_default_variables() {
  return array(
    'commerce_barcode_scanner_min_sku_length' => '5',
    'commerce_barcode_scanner_key_interval' => '50',
  );
}


/**
 * Get a product display given a product.
 *
 * @param stdClass $product
 *   The product to lookup the display for.
 *
 * @return bool|mixed
 *   A product display or false on failure.
 */
function commerce_barcode_scanner_display_referencing_product($product) {
  foreach (commerce_info_fields('commerce_product_reference') as $field) {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node', '=')
      ->fieldCondition($field['field_name'], 'product_id', $product->product_id, '=')
      ->range(0, 1);
    if ($result = $query->execute()) {
      $node_keys = array_keys($result['node']);
      return array_shift($node_keys);
    }
  }
  return FALSE;
}
